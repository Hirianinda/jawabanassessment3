/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jawabanassessment8;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ZANDUT
 */
public class Departement implements Serializable
{
    private String name;
    private Set<Employee> arrayEmployee = new HashSet<>();
        
    public void addEmployee(Employee e)
    {
        arrayEmployee.add(e);
    }
    
    public Employee getEmployee(int n)
    {
        Object[] array = arrayEmployee.toArray();
        return (Employee) array[n];
    }

  

    public Departement()
    {
        
    }
    
   
    
    public void removeEmployee(int n)
    {
        arrayEmployee.remove(getEmployee(n));
    }
    
    public void showAllEmployee()
    {
        System.out.println("===Daftar Employee===");
        for (Employee obj : arrayEmployee)
        {
            
            System.out.println("Name : "+obj.getName());
            System.out.println("ID : "+obj.getIdEmployee());
            System.out.println("Salary : "+obj.getSalary());
            System.out.println("====================================");
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public Departement(String name)
    {
        this.name = name;
    }
    
    //innes class Employee
    public class Employee implements Serializable
    {
        private String name;
        private long idEmployee;
        private double salary;

        public Employee(String name, long idEmployee, double salary)
        {
            this.name = name;
            this.idEmployee = idEmployee;
            this.salary = salary;
        }

        public void setIdEmployee(long idEmployee)
        {
            this.idEmployee = idEmployee;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public void setSalary(double salary)
        {
            this.salary = salary;
        }

        public long getIdEmployee()
        {
            return idEmployee;
        }

        public String getName()
        {
            return name;
        }

        public double getSalary()
        {
            return salary;
        }
        
        
    }
    
}
